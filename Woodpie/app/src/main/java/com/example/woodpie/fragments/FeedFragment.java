package com.example.woodpie.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
//import android.support.v4.app.Fragment;
//import android.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.woodpie.R;
import com.example.woodpie.adapters.FeedListAdapter;
import com.example.woodpie.data.FeedItem;
import com.example.woodpie.entities.Book;
import com.example.woodpie.entities.User;
import com.example.woodpie.listeners.InfiniteScrollListener;
import com.example.woodpie.enums.ActionType;

import java.util.ArrayList;
import java.util.List;

public class FeedFragment extends Fragment {

    private ListView listView;
    private FeedListAdapter
            listAdapter;
    private List<FeedItem> feedItems;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.feed_fragment, container, false);
        listView = (ListView) view.findViewById(R.id.list);

        // setting up font awesome fonts
        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fontawesome-webfont.ttf");

        feedItems = new ArrayList<>();

        listAdapter = new FeedListAdapter(getActivity(), feedItems, font);
        listView.setAdapter(listAdapter);
        listView.setOnScrollListener(new InfiniteScrollListener() {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                addMoreFeed();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getActivity(), "Clicked a post", Toast.LENGTH_SHORT).show();
            }
        });

        addFeed();

        return view;
    }

    private void addFeed()
    {
        for (int i = 0; i < 5; i++)
        {
            FeedItem feedItem = new FeedItem();
            User user = new User();
            user.setUserName("Asmita Metrewar");
            user.setProfilePicURL("https://qph.is.quoracdn.net/main-thumb-9289996-200-jbqkkqweisqhwcvyzhghtqtmvaneprwd.jpeg");
            feedItem.setUser(user);

            Book book = new Book();
            book.setBookName("To Kill A Mockingbird");
            book.setAuthorName("Harper Lee");
            book.setCoverPicURL("http://ecx.images-amazon.com/images/I/51J6fU-Lw%2BL.jpg");
            feedItem.setBook(book);

            feedItem.setId(i);
            feedItem.setAction(ActionType.ADD_TO_COLLECTION);

            for (int j = 0; j < 4; j++)
                feedItem.getComments().add("Comment " + j);
            feedItem.setLikes(9);

            feedItems.add(feedItem);
        }
        listAdapter.notifyDataSetChanged();
    }

    private void addMoreFeed()
    {
        for (int i = 0; i < 10; i++)
        {
            FeedItem feedItem = new FeedItem();
            User user = new User();
            user.setUserName("Sherlock Holmes");
            user.setProfilePicURL("http://www.bbc.co.uk/pressoffice/images/bank/programmes_tv/drama/sherlock/446benedict_cumberbatch.jpg");
            feedItem.setUser(user);

            Book book = new Book();
            book.setBookName("A Game of Thrones");
            book.setAuthorName("George R R Martin");
            book.setCoverPicURL("http://www.georgerrmartin.com/wp-content/uploads/2013/03/GOTMTI2.jpg");
            feedItem.setBook(book);

            feedItem.setId(feedItems.size());
            feedItem.setAction(ActionType.RATE);
            feedItem.setRating(4);
            feedItem.setLiked(true);
            feedItem.setLikes(6);

            feedItems.add(feedItem);
        }
        listAdapter.notifyDataSetChanged();
    }
}
