package com.example.woodpie.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.woodpie.utils.ImageWorker;
import com.example.woodpie.R;
import com.example.woodpie.data.FeedItem;
import com.example.woodpie.enums.ActionType;

import org.apache.http.HttpStatus;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;


public class FeedListAdapter extends BaseAdapter
{
    private Activity activity;
    private LayoutInflater inflater;
    private List<FeedItem> feedItems;
    ImageWorker worker;
    private Typeface font;

    public FeedListAdapter(Activity activity, List<FeedItem> feedItems, Typeface font) {
        this.activity = activity;
        this.feedItems = feedItems;
        this.font = font;
    }


    @Override
    public int getCount() {
        return feedItems.size();
    }

    @Override
    public Object getItem(int position) {
        return feedItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.feed_item_view, null);

        ImageView profilePic = (ImageView) convertView.findViewById(R.id.profile_pic);
        TextView userName = (TextView) convertView.findViewById(R.id.user_name);
        TextView action = (TextView) convertView.findViewById(R.id.action);
        ImageView bookCover = (ImageView) convertView.findViewById(R.id.book_cover);
        TextView bookName = (TextView) convertView.findViewById(R.id.book_name);
        TextView bookAuthor = (TextView) convertView.findViewById(R.id.book_author);
        TextView bookReview = (TextView) convertView.findViewById(R.id.review);
        RatingBar bookRating = (RatingBar) convertView.findViewById(R.id.user_rating);

        TextView numberOfLikes = (TextView) convertView.findViewById(R.id.number_of_likes);
        TextView numberOfComments = (TextView) convertView.findViewById(R.id.number_of_comments);
        TextView numberOfLikesCommentsSeparator = (TextView) convertView.findViewById(R.id.number_of_likes_comments_separator);

        TextView likeText = (TextView) convertView.findViewById(R.id.like_text);
        TextView likeIcon = (TextView) convertView.findViewById(R.id.like_icon);
        likeIcon.setTypeface(font);

        TextView commentIcon = (TextView) convertView.findViewById(R.id.comment_icon);
        commentIcon.setTypeface(font);

        numberOfLikesCommentsSeparator.setTypeface(font);

        LinearLayout likeLayout = (LinearLayout) convertView.findViewById(R.id.like_button);

        final View finalConvertView = convertView;
        likeLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                final TextView text = (TextView) v.findViewById(R.id.like_text);
                final TextView like = (TextView) v.findViewById(R.id.like_icon);
                if (like.getText().toString().equalsIgnoreCase(activity.getResources().getString(R.string.like_button_icon_solid))) {
                    text.setTypeface(null, Typeface.NORMAL);
                    text.setTextColor(activity.getResources().getColor(R.color.default_gray));
                    like.setText(R.string.like_button_icon_line);
                    like.setTextColor(activity.getResources().getColor(R.color.default_gray));
                    feedItems.get(position).setLiked(false);
                    feedItems.get(position).setLikes(feedItems.get(position).getLikes() - 1);
                } else {
                    text.setTypeface(null, Typeface.BOLD);
                    text.setTextColor(activity.getResources().getColor(R.color.like_liked));
                    like.setText(R.string.like_button_icon_solid);
                    like.setTextColor(activity.getResources().getColor(R.color.like_liked));
                    feedItems.get(position).setLiked(true);
                    feedItems.get(position).setLikes(feedItems.get(position).getLikes() + 1);
                }
                updateLikeCommentCount(finalConvertView, feedItems.get(position));
            }
        });


        FeedItem feedItem = feedItems.get(position);

        final Bitmap[] profilePicBmp = {null};
        final String profilePicURL = feedItem.getUser().getProfilePicURL();
        if (profilePicURL != null) {
            worker = new ImageWorker(activity) {
                @Override
                protected Bitmap processBitmap(Object data) {
                    Log.d("Mack", "returning null");
                    HttpURLConnection urlConnection = null;
                    try {
                        URL url = new URL(data.toString());
                        urlConnection = (HttpURLConnection) url.openConnection();

                        int statusCode = urlConnection.getResponseCode();
                        if (statusCode != HttpStatus.SC_OK)
                            return null;

                        InputStream inputStream = urlConnection.getInputStream();
                        if (inputStream != null) {
                            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                            return bitmap;
                        }
                    } catch (Exception e) {
                        Log.w("ImageDownloader", "Error downloading image from " + data.toString());
                    } finally {
                        if (urlConnection != null)
                            urlConnection.disconnect();
                    }
                    return null;
                }

            };
            final Bitmap bitmap = worker.loadImage(profilePicURL, profilePic);
            if (bitmap != null) {
                Log.d("Mack", "bitmap not null");
                profilePic.setImageBitmap(bitmap);

            }

            userName.setText(feedItem.getUser().getUserName());
            action.setText(feedItem.getAction().getActionText());

            final Bitmap[] bookCoverBmp = {null};
            final String coverPicURL = feedItem.getBook().getCoverPicURL();
            if (coverPicURL != null) {
                worker = new ImageWorker(activity) {
                    @Override
                    protected Bitmap processBitmap(Object data) {
                        Log.d("Mack", "returning null");
                        HttpURLConnection urlConnection = null;
                        try {
                            URL url = new URL(data.toString());
                            urlConnection = (HttpURLConnection) url.openConnection();

                            int statusCode = urlConnection.getResponseCode();
                            if (statusCode != HttpStatus.SC_OK)
                                return null;

                            InputStream inputStream = urlConnection.getInputStream();
                            if (inputStream != null) {
                                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                                return bitmap;
                            }
                        } catch (Exception e) {
                            Log.w("ImageDownloader", "Error downloading image from " + data.toString());
                        } finally {
                            if (urlConnection != null)
                                urlConnection.disconnect();
                        }
                        return null;
                    }
                };
                final Bitmap bitmap1 = worker.loadImage(coverPicURL, bookCover);
                if (bitmap1 != null) {
                    Log.d("Mack", "bitmap not null");
                    bookCover.setImageBitmap(bitmap1);
                }/* else {
                mImageView.setImageResource(R.drawable.image_placeholder);
                BitmapWorkerTask task = new BitmapWorkerTask(mImageView);
                task.execute(resId);
            }*/
            }

            bookName.setText(feedItem.getBook().getBookName());
            bookAuthor.setText("By " + feedItem.getBook().getAuthorName());

            updateLikeCommentCount(convertView, feedItem);

            if (feedItem.getAction().equals(ActionType.REVIEW)) {
                bookReview.setText(feedItem.getReview());
                bookRating.setProgress(feedItem.getRating());
                bookRating.setVisibility(View.VISIBLE);
                bookReview.setVisibility(View.VISIBLE);
            } else if (feedItem.getAction().equals(ActionType.RATE)) {

            /*
            int rating =  feedItem.getRating();
            this.setStars(rating);
            Drawable progress = bookRating.getProgressDrawable();
            DrawableCompat.setTint(progress, Color.parseColor("#800000"));
            */

                bookRating.setRating(feedItem.getRating());
                bookRating.setVisibility(View.VISIBLE);
                bookReview.setVisibility(View.VISIBLE);
            } else {
                bookRating.setVisibility(View.GONE);
                bookReview.setVisibility(View.GONE);
            }

            if (feedItem.isLiked()) {
                likeIcon.setText(R.string.like_button_icon_solid);
                likeIcon.setTextColor(this.activity.getApplicationContext().getResources().getColor(R.color.like_liked));
                likeText.setTypeface(null, Typeface.BOLD);
                likeText.setTextColor(this.activity.getApplicationContext().getResources().getColor(R.color.like_liked));
            }
            else {
                likeIcon.setText(R.string.like_button_icon_line);
                likeIcon.setTextColor(this.activity.getApplicationContext().getResources().getColor(R.color.default_gray));
                likeText.setTypeface(null, Typeface.NORMAL);
                likeText.setTextColor(this.activity.getApplicationContext().getResources().getColor(R.color.default_gray));
            }

            if (profilePicBmp[0] != null)
                profilePic.setImageBitmap(profilePicBmp[0]);
            if (bookCoverBmp[0] != null)
                bookCover.setImageBitmap(bookCoverBmp[0]);


            return convertView;
        }
        return  null;
    }



    private void updateLikeCommentCount(View convertView, FeedItem feedItem) {
        TextView numberOfLikes = (TextView) convertView.findViewById(R.id.number_of_likes);
        TextView numberOfComments = (TextView) convertView.findViewById(R.id.number_of_comments);
        TextView numberOfLikesCommentsSeparator = (TextView) convertView.findViewById(R.id.number_of_likes_comments_separator);

        if (feedItem.getLikes() > 0 || feedItem.getComments().size() > 0) {
            if (feedItem.getLikes() > 0) {
                numberOfLikes.setVisibility(View.VISIBLE);
                numberOfLikes.setText(feedItem.getLikes() + " likes");
            }
            else {
                numberOfLikes.setVisibility(View.GONE);
                numberOfLikesCommentsSeparator.setVisibility(View.GONE);
            }
            if (feedItem.getComments().size() > 0) {
                if (feedItem.getLikes() > 0) {
                    numberOfLikesCommentsSeparator.setVisibility(View.VISIBLE);
                }
                else {
                    numberOfLikesCommentsSeparator.setVisibility(View.GONE);
                }
                numberOfComments.setVisibility(View.VISIBLE);
                numberOfComments.setText(feedItem.getComments().size() + " comments");
            }
            else {
                numberOfLikesCommentsSeparator.setVisibility(View.GONE);
                numberOfComments.setVisibility(View.GONE);
            }
        }
        else {
            numberOfLikes.setVisibility(View.GONE);
            numberOfLikesCommentsSeparator.setVisibility(View.GONE);
            numberOfComments.setVisibility(View.GONE);
        }

    }
}
