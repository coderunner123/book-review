package com.example.woodpie.enums;

public enum IntentKeys {
    FRAGMENT_NAME("fragmentName"),
    USER_NAME("userName"),
    USER_PIC("userPic")
    ;

    String key;

    private IntentKeys(String key) { this.key = key; }
    public String getKey() { return key; }
}
