package com.example.woodpie.activities;

import android.content.Intent;
import android.content.IntentSender;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.woodpie.R;
import com.example.woodpie.enums.FragmentNames;
import com.example.woodpie.enums.IntentKeys;
import com.example.woodpie.utils.Constants;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.Plus.PlusOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.IOException;
import java.util.Arrays;

import cz.msebera.android.httpclient.Header;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener,
        ConnectionCallbacks,OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getCanonicalName();

    // G+ related things
    private SignInButton gPlusLoginButton;
    private static final int RC_SIGN_IN = 0;

    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    private static final int ERROR_DIALOG_REQUEST_CODE = 0;

    private final double hratio = 0.09722224;
    private final double wratio = 0.59949833;
    // fb related things
//    private LoginButton fbLoginButton;
    private CallbackManager callbackManager;

    Button fbLoginButton;
    Button googleLoginButton;
 
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // FB related things
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        setContentView(R.layout.activity_login);

        // G+ related things

        googleLoginButton = (Button)findViewById(R.id.google_local_button);
        googleLoginButton.setOnClickListener(this);
 
        mGoogleApiClient = buildGoogleAPIClient();


        // fb related things

        fbLoginButton = (Button) findViewById(R.id.fb_local_button);
        fbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginManager.getInstance().
                        logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "user_friends"));
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Toast.makeText(getApplicationContext(), R.string.welcome_on_login, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        intent.putExtra(IntentKeys.FRAGMENT_NAME.getKey(), FragmentNames.FEED);

                        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                            @Override
                            protected String doInBackground(Void... params) {
                                String token = null;
                                // get access token using FB api
                                try {
                                    token = AccessToken.getCurrentAccessToken().getToken();
                                    if (token != null)
                                        sendAccessTokenToServer(token);
                                } catch (Exception e) {
                                    Log.e(TAG, e.getMessage());
                                }
                                return token;
                            }

                            @Override
                            protected void onPostExecute(String token) {
                                Log.i(TAG, "Access token retrieved:" + token);

                            }
                        };
                        task.execute();

                        Profile profile = Profile.getCurrentProfile();
                        if (profile != null) {
                            intent.putExtra(IntentKeys.USER_NAME.getKey(), profile.getName());
                            intent.putExtra(IntentKeys.USER_PIC.getKey(), profile.getProfilePictureUri(20, 20).getPath());
                        } else {
                            Log.e(TAG, "Profile is null.");
                        }

                        LoginActivity.this.finish();
                        startActivity(intent);

                    }

                    @Override
                    public void onCancel() {
                        Toast.makeText(getApplicationContext(), "Login cancelled!", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onError(FacebookException e) {
                        Toast.makeText(getApplicationContext(), "Login failed!", Toast.LENGTH_SHORT).show();

                    }
                });
    }
  @Override
   protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private GoogleApiClient buildGoogleAPIClient() {
        return new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API, PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_PROFILE).build();
    }


    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
    }

    @Override
    public void onClick(View v) {
        signInWithGplus();
    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            processSignInError();
        }
    }

    private void processSignInError() {
        Log.e(TAG, "In resolveSignInError.");
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {

        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    ERROR_DIALOG_REQUEST_CODE).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = result;

            if (mSignInClicked) {
                processSignInError();
            }

        }

    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mSignInClicked = false;
        Toast.makeText(getApplicationContext(), R.string.welcome_on_login,
                Toast.LENGTH_LONG).show();

        // get access token and send it to the backend server

        AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                    String token = null;
                    final String SCOPES = "https://www.googleapis.com/auth/plus.login ";
                    // get access token using G+ api
                    try {
                        token = GoogleAuthUtil.getToken(getApplicationContext(),
                                Plus.AccountApi.getAccountName(mGoogleApiClient),
                                "oauth2:" + SCOPES);
                        sendAccessTokenToServer(token);
                    } catch (IOException e) {
                        Log.e(TAG, e.getMessage());
                    } catch (GoogleAuthException e) {
                        Log.e(TAG, e.getMessage());
                    }
                return token;
            }

            @Override
            protected void onPostExecute(String token) {
                Log.i(TAG, "Access token retrieved:" + token);

            }
        };
        task.execute();

        // Go to feed
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.putExtra(IntentKeys.FRAGMENT_NAME.getKey(), FragmentNames.FEED);
        LoginActivity.this.finish();
        startActivity(intent);

    }

    private void sendAccessTokenToServer(String token) {
        Log.i(TAG, "Sending access token to server in sendAccessTokenToServer " + token);
        try {
            // send access token to the backend server
            AsyncHttpClient client = new AsyncHttpClient();
            RequestParams requestParams = new RequestParams();
            requestParams.put("access_token", token);

            Looper.prepare();

            client.post(Constants.serverURL, requestParams, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    Log.d(TAG, "onSuccess: " + responseBody.toString());
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.d(TAG, "onFailure: " + responseBody.toString());
                }
            });
        }
        catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }


    @Override
    public void onConnectionSuspended(int cause) {
        mGoogleApiClient.connect();

    }
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        //Here you can get the size!
       Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width  = size.x;
        int height = size.y;

        googleLoginButton.setWidth((int)((float)width * wratio));
        googleLoginButton.setHeight((int) ((float) height * hratio));

        Log.d("Mack","Dimension Width:-"+googleLoginButton.getWidth()+"\t Height"+googleLoginButton.getHeight());

        fbLoginButton.setWidth((int)((float)width * wratio));
        fbLoginButton.setHeight((int)((float)height * hratio));

        Log.d("Mack","Dimension Width:-"+fbLoginButton.getWidth()+"\t Height"+fbLoginButton.getHeight());
    }
}


