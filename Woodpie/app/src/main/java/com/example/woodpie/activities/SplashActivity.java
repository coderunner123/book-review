package com.example.woodpie.activities;

import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.example.woodpie.R;
import com.example.woodpie.utils.NetworkManager;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.FacebookSdk;

public class SplashActivity extends AppCompatActivity {

    static ConnectivityManager cm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        getSupportActionBar().hide();

        if (NetworkManager.isConnected(this)) {
            Thread timerThread = new Thread() {
                public void run() {
                    try {
                        sleep(3000);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finally {
                        SplashActivity.this.finish();

                        FacebookSdk.sdkInitialize(getApplicationContext());
                        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
                            @Override
                            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken newAccessToken) {
                                updateWithToken(newAccessToken);
                            }
                        };
                        accessTokenTracker.startTracking();
                        updateWithToken(AccessToken.getCurrentAccessToken());
                    }
                }
            };
            timerThread.start();
        }
        else {
            Toast.makeText(SplashActivity.this, R.string.not_connected, Toast.LENGTH_SHORT).show();
        }

    }

    private void updateWithToken(AccessToken accessToken) {
        if (accessToken != null) {
            Intent intent = new Intent(SplashActivity.this, MainActivity.class);
            startActivity(intent);
        }
        else {
            Log.e(SplashActivity.class.getCanonicalName(), "Access token is null!");
            Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
